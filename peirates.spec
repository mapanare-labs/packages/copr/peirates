%define debug_package %{nil}

Name:           peirates
Version:        1.1.25
Release:        0%{?dist}
Summary:        Kubernetes Penetration Testing tool

License:        ASL 2.0
URL:            https://www.inguardians.com/peirates/
Source0:        https://github.com/inguardians/peirates/releases/download/v%{version}/peirates-linux-amd64.tar.xz

%description
Kubernetes Penetration Testing tool

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 peirates-linux-amd64/%{name} %{buildroot}/usr/bin

%files
/usr/bin/peirates

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.1.16

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.1.15

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.1.12

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.1.11

* Thu Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM